# Liblast, a libre multiplayer FPS game
Copyleft (ɔ) 2020-2021 Liblast Team, Liblast contributors

## Liblast Team members

- Tobiasz 'unfa' Karoń
- Pablo 'Pablinski2' Pozo
- Jan 'Combustible Lemonade' Heemstra
- Adham 'gilgamesh' Omran
- John 'nebunez' Ryan

### Inactive Team members

- JM 'aRdem' (concept art, character design, level design)

## Liblast contributors

(none)

### Inactive contributors

(none)