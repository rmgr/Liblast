extends Node3D

func _ready():
	for i in get_children(): # activate all top-level particle systems secondary ones should be parented to the primary ones
		if i is GPUParticles3D:
			i.emitting = true
			#print ("activating ", i)
	#self.look_at(get_viewport().get_camera_3d().global_transform.origin)
	#$MeshInstance3D.get_active_material(0).set("shader_params/o3007790_OFFSET", randf_range(0, 1))
	
func _on_Timer_timeout():
	queue_free()
