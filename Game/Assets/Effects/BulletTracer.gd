extends Node3D

const SPEED = 250

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	#translate_object_local(Vector3.FORWARD * SPEED / 60 * randf_range(0, 1)) # randomize starting point
	$RayCast3D.target_position = - Vector3.FORWARD * SPEED  / 30

func _process(delta):
	if $RayCast3D.is_colliding():
		queue_free()
	else:
		translate_object_local(Vector3.FORWARD * SPEED * delta)
