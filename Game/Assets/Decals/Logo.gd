extends Decal

@onready var main = get_tree().root.get_node("Main")

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	emission_energy = sin(main.uptime * 2) + 1
