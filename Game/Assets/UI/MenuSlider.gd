extends "res://Assets/UI/MenuData.gd"

func set_data(_data):
	super.set_data(_data)
	$Slider.value = _data

func on_label_changed():
	$Label.text = label

func on_value_changed(value):
	data = value
	$ClickSound.play()

func _on_Slider_mouse_entered():
	$HoverSound.play()
