extends "res://Assets/UI/Menu.gd"

func on_visibility_changed():
	if visible:
		$SuicideButton.visible = (Main.role != Main.MultiplayerRole.NONE)

func commit_suicide():
	Main.commit_suicide()

func host_button_pressed():
	Main.host_server()
