extends "res://Assets/UI/MenuItem.gd"

@export var index = ""
@onready var GUI = get_parent().get_parent().get_parent()

signal data_changed(data)

func _ready():
	if index in GUI.settings.keys():
		set_data(GUI.settings[index])

func set_data(_data):
	if GUI == null:
		GUI = get_parent().get_parent()
	
	data = _data

var data = null:
	set(_data):
		emit_signal("data_changed", _data)
		data = _data
		save_data()

func save_data():
	GUI.settings[index] = data
	GUI.save_settings()
