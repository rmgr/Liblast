extends Control

var settings_filename = "user://settings.save"
var settings = {}

var map: Node3D

func _ready():
	if has_settings():
		load_settings()
		apply_settings()
	else: # apply defaults
		settings = {'Fullscreen':true,
					'Sensitivity':0.5,
					'ip':'unfa.xyz',
					'player color':Color(1, 1, 1, 1),
					'username':'player',
					'GraphicsRenderScale':1.0,
					}
		apply_settings()
		save_settings()
		
func has_settings():
	var filecheck = File.new()
	return filecheck.file_exists(settings_filename)

func set_setting(key, value):
	settings[key] = value
	save_settings()

func save_settings():
	var file = File.new()
	file.open(settings_filename, File.WRITE)
	file.store_var(settings)
	file.close()

func load_settings():
	var file = File.new()
	file.open(settings_filename, File.READ)
	settings = file.get_var()
	file.close()
	
	if typeof(settings) != TYPE_DICTIONARY:
		settings = {}

func apply_settings():
	# get the currently loaded map
	map = get_tree().get_root().get_node("Main").get_node("Map")
	
	for key in settings.keys():
		match key:
			"Sensitivity":
				set_mouse_sensitivity(settings[key])
			"GraphicsFullscreen":
				set_fullscreen(settings[key])
			"GraphicsAmbientOcclusion":
				set_ambient_occlusion(settings[key])
			"GraphicsShadows":
				set_shadows(settings[key])
			"GraphicsGlobalIllumination":
				set_global_illumination(settings[key])
			"GraphicsScreenSpaceReflections":
				set_screen_space_reflections(settings[key])
			"GraphicsReflectionProbes":
				set_reflection_probes(settings[key])
			"GraphicsGlow":
				set_glow(settings[key])
			"GraphicsMSAA":
				set_MSAA(settings[key])
			"GraphicsFXAA":
				set_FXAA(settings[key])
			"GraphicsDebanding":
				set_debanding(settings[key])
			"GraphicsDecalsStatic":
				set_decals_static(settings[key])
			"GraphicsDecalsDecals":
				set_decals_decals(settings[key])
			"GraphicsDynamicLights":
				set_dynamic_lights(settings[key])

# These functions are for applying settings changes
func quit_game():
	
	var main = get_tree().get_root().get_node("Main")
	var pid = main.get_multiplayer_authority()
	var player_node = main.get_node("Players").get_node(str(pid))
	
	#main.get_node("HUD/Chat").rpc(&'chat_notification', "Player " + str(pid) +" left the game.")
	#player_node.rpc(&'die', -1)
	#main.rpc(&'disconnect_player', pid) # tell everyone we're leaving
	#main.multiplayer = null
	
	get_tree().multiplayer.multiplayer_peer = null
	
	get_tree().quit()

func set_mouse_sensitivity(sensitivity):
	pass

func set_fullscreen(is_fullscreen):
	if is_fullscreen:
		get_tree().get_root().mode = Window.MODE_FULLSCREEN
	else:
		get_tree().get_root().mode = Window.MODE_WINDOWED

func set_ambient_occlusion(is_enabled):
	# TODO
	pass

func set_shadows(is_enabled):
	# TODO
	pass

func set_global_illumination(is_enabled):
	# TODO
	pass

func set_screen_space_reflections(is_enabled):
	# TODO
	pass

func set_reflection_probes(is_enabled):
	# TODO
	pass

func set_glow(is_enabled):
	# TODO
	pass

func set_MSAA(is_enabled):
	# TODO
	pass

func set_FXAA(is_enabled):
	# TODO
	pass

func set_debanding(is_enabled):
	# TODO
	pass

func set_decals_static(is_enabled):
	# TODO
	pass

func set_decals_decals(is_enabled):
	# TODO
	pass

func set_dynamic_lights(is_enabled):
	# TODO
	pass

func set_render_scale(value):
	get_viewport().scaling_3d_scale = value
	
func set_fsr(is_enabled):
	get_viewport().scaling_3d_mode = Viewport.SCALING_3D_MODE_FSR if is_enabled else Viewport.SCALING_3D_MODE_BILINEAR
