extends RigidDynamicBody3D

@onready var sound = $Sound
@onready var sound_player = sound.get_node("AudioStreamPlayer3D")

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	var mat = $Casing/Casing_LOD0.get_active_material(0).duplicate()
	$Casing/Casing_LOD0.set_surface_override_material(0, mat)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Timer_timeout():
	$AnimationPlayer.play("Fade")

func _on_Casing_body_entered(body):
	var vel = linear_velocity.length()
	#print(linear_velocity.length())
	
	if vel > 1:
		sound_player.unit_db = -48 + min((pow(vel, 3)), 48)
		sound.play()
