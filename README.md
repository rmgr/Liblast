<p align="center"><img src="https://codeberg.org/unfa/liblast/media/branch/main/Design/Logo.png" alt="Liblast - a Libre Multiplayer FPS Game"></p>

# Liblast

A Libre Multiplayer FPS Game built with Godot game engine and a fully FOSS toolchain.

![Screenshot](https://codeberg.org/unfa/Liblast/raw/commit/b44a7e892bf03d896f914f87bb44e1a192f1fbc0/Screenshots/0.1.1.1_playtesting.jpg)

Primary goals:

1. Create a fun open-source game for everyone to enjoy
2. Prove that libre creative tools suffice to make a good game - we use only open-source software in the production
3. Have fun, get to know each other and grow together with the project

Secondary goals:

1. Push the envelope of open-source FPS games in regards to a coherent design, style, technology and overall quality
2. Enable the game to be playable on lower-end computers, but provide higher fidelity for those who have more powerful hardware
3. Allow joining the game from a web browser for quick sessions, as well as providing downloadable package for residential or portable usage
4. Actively help the development of Godot engine and other open-source tools that we depend on
5. Facilitate 3rd party content via mods
6. Provide optional online accounts to securely store user data and protect their in-game identity

---

Check out the [YouTube channel](https://www.youtube.com/channel/UC1Oi1eXwdr8RlqIslyht5AQ) for video updates and livestreamed playtesting sessions.

## Join us!

This is a non-profit project, created and run by hobbyists. We're having fun working together, though we need your help to bring this game into reality, and show the world what the open-source game development is relly made of!

### Who are we looking for?

- An Art Lead (help us create a unique and memorable aesthetic for the game!)
- GDScript Programmers (help us implement all the features and squash the bugs!)
- Concept artists (help us come up with awesome character, weapon, and environment designs!)
- Playtesters (help us make sure the game is fun!)
- Blender 3D Artsits (help us made the 3D world of the game!)
- Material Maker Artists (help us create awesome PBR materials and VFX!)
- Writers (help us develop the lore of the game and make sure the world is consistent!)
- Voice actors (help us make the characters unforgettale and lovable!)

Even if ou don't have much experience, but you are dediated to learn the art of game development and want to make something awesome together - get in touch!

### Contact the team:

- https://mastodon.social/@unfa or `unfa00 at google dot com` (project lead)
- https://chat.unfa.xyz/channel/liblast (public chat, also used internally by the team)


---

## How To Play

### Download the game

Go to the [releases page](https://codeberg.org/unfa/liblast/releases) and download the latest release of the game. You'll find some instructions and notes there as well.

There's one public dedicated server running at `liblast.unfa.xyz` the game will present the server address upon startup.

To start playing Liblast it's recommended to first host a local game and adjust your mouse sensitivity and other preferences, as well as modify your profle (player name and color). Then connect to a server and play!

### Controls

| Key          | Action         |
|--------------|----------------|
| WASD         | Movement       |
| Mouse        | Camera         |
| Left Click   | Shoot          |
| Space        | Jump           |
| Shift (Hold) | Jetpack        |
| 1 / 2        | Select weapon  |
| T            | Chat with Team |
| Y            | Chat with All  |
| Z            | Zoom           |
| M            | Mute audio     |
| ESC          | Main Menu      |


## Contributing and Getting in touch

Check the [Contribution Guide!](./CONTRIBUTING.md)

If you want to talk to the dev team and discuss the game in an instant manner, go here:
https://chat.unfa.xyz/channel/liblast

## How to Edit the Game

### Get the Godot 4 editor

As Godot 4 haven't had a stable release yet, we're using the development builds achived at TuxFamily.
It's important that everyone uses the same version of the engine, as there's still rapid changes going on.

[Download the latest Godot editor executables and export templates as needed.](https://downloads.tuxfamily.org/godotengine/testing/4.0/)

If you're having issues and you suspect an engine bug, [test with the current nightly builds](https://hugo.pro/projects/godot-builds/
) before reporing the problem to here or in [Godot Github](https://github.com/godotengine/godot).


### GNU/Linux

1. Make sure you have `git` and `git-lfs` installed. Additional tools to make using `git` in the commandline easier that we can recommend are [`tig`](https://github.com/jonas/tig) and [`lazygit`](https://github.com/jesseduffield/lazygit).

2. Clone the Git repository:
```
git clone https://codeberg.org/unfa/liblast.git
```

3. Enter the cloned repository:
```
cd liblast
```

4. Initialize Git-LFS:
```
git lfs install
```

5. Pull the Git-LFS stored files:
```
git lfs pull
git fetch
```

6. Run the Godot editor and import the project located in `liblast/Game/project.godot`


### Windows (untested!)

1. Install Git for Windows: https://gitforwindows.org/


2. Clone the Git repository:
```
git clone https://codeberg.org/unfa/liblast.git
```

3. Open GitBash in the cloned repository (ther should be an option in the context menu in Windows Explorer)


4. Initialize Git-LFS:
```
git lfs install
```

5. Pull the Git-LFS stored files (that includes the bundled Godot editor):
```
git lfs pull
git fetch
```

6. Run the Godot editor and import the project located in `../Game/project.godot`


## What does the name of this project mean?

`Libre` + `Blast` = `Liblast` (pronounced _ˈlɪblɑːst_)

No, it's not a library ;)